package com.qcs.configserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@EnableConfigServer
@SpringBootApplication
public class QCSConfigServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(QCSConfigServerApplication.class, args);
	}
}
