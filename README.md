## QCS Config Server
- http://localhost:9999/actuator/health
- update or add new properteis to the config-server-client.properties (default)
- update or add new properteis to the config-server-client-development.properties (development)
- update or add new properteis to the config-server-client-production.properties (production)
- you can make any update to those file and then check it into git
- you then can call the endpoints below to verify your changes WITHOUT refreshing/restarting the config server
    - this is defined in the resources/bootstrap.properties file by calling the actual git repository for this config server
- http://localhost:9999/config-server-client/development
- http://localhost:9999/config-server-client/production

## Deploy to Heroku
- using heroku maven plugin on root pom.xml
- cd to project root directory
- mvn clean heroku:deploy 
- heroku ps:scale web=1 --app qcs-config-server
- https://qcs-config-server.herokuapp.com/actuator/health

# few config endpoints for qcs-stats-service
- https://qcs-config-server.herokuapp.com/qcs-development/dev
- https://qcs-config-server.herokuapp.com/qcs-production/dev
- https://qcs-config-server.herokuapp.com/qcs-qa/dev
- https://qcs-config-server.herokuapp.com/qcs/dev
- --------
- https://qcs-config-server.herokuapp.com/qcs/dev
- https://qcs-config-server.herokuapp.com/qcs/qa
- https://qcs-config-server.herokuapp.com/qcs/production